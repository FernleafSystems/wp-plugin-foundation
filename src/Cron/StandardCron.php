<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Cron;

use FernleafSystems\Wordpress\Plugin\Foundation\Utility\PrefixConsumer;
use FernleafSystems\Wordpress\Services\Services;

trait StandardCron {

	use PrefixConsumer;

	/**
	 * @var int
	 */
	private $nCronFirstRun;

	/**
	 * Use to setup the cron
	 */
	public function init() {
		$this->setupCron();
	}

	protected function setupCron() {
		try {
			Services::WpCron()
					->setRecurrence( $this->getCronRecurrence() )
					->setNextRun( $this->getFirstRunTimestamp() )
					->createCronJob( $this->getCronName(), [ $this, 'runCron' ] );
		}
		catch ( \Exception $oE ) {
		}
		add_action( $this->getPrefix()->prefix( 'deactivate_plugin' ), [ $this, 'deleteCron' ] );
	}

	/**
	 * @return string
	 */
	protected function getCronRecurrence() {
		return $this->getCronFrequency();
		/* TODO: add support for our standardised custom schedules
		$aStdIntervals = array_keys( wp_get_schedules() );
		return in_array( $sFreq, $aStdIntervals ) ?
			$sFreq
			: $this->getCon()->prefix( sprintf( 'per-day-%s', $sFreq ) ); */
	}

	/**
	 * @return int|string
	 */
	protected function getCronFrequency() {
		return 'daily';
	}

	/**
	 * @return string
	 */
	abstract protected function getCronName();

	/**
	 * @return int
	 */
	public function getFirstRunTimestamp() {
		return empty( $this->nCronFirstRun ) ? ( Services::Request()->ts() + MINUTE_IN_SECONDS ) : $this->nCronFirstRun;
	}

	/**
	 * @return int
	 */
	protected function getNextCronRun() {
		$nNext = wp_next_scheduled( $this->getCronName() );
		return is_numeric( $nNext ) ? $nNext : 0;
	}

	public function deleteCron() {
		Services::WpCron()->deleteCronJob( $this->getCronName() );
	}

	protected function resetCron() {
		$this->deleteCron();
		$this->setupCron();
	}

	public function runCron() {
		// Override to run the actual Cron activity
	}

	/**
	 * @param int $nFirstRun
	 * @return $this
	 */
	public function setFirstRun( $nFirstRun ) {
		$this->nCronFirstRun = $nFirstRun;
		return $this;
	}
}