<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Cron;

class DailyCron {

	use StandardCron;

	/**
	 * @return string
	 */
	protected function getCronFrequency() {
		return 'daily';
	}

	/**
	 * @return string
	 */
	protected function getCronName() {
		return $this->getPrefix()->prefix( 'daily' );
	}

	/**
	 * Use the included action to hook into the plugin's daily cron
	 */
	public function runCron() {
		do_action( $this->getPrefix()->prefix( 'daily_cron' ) );
	}
}