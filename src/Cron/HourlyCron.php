<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Cron;

class HourlyCron {

	use StandardCron;

	/**
	 * @return string
	 */
	protected function getCronFrequency() {
		return 'hourly';
	}

	/**
	 * @return string
	 */
	protected function getCronName() {
		return $this->getPrefix()->prefix( 'hourly' );
	}

	/**
	 * Use the included action to hook into the plugin's daily cron
	 */
	public function runCron() {
		do_action( $this->getPrefix()->prefix( 'hourly_cron' ) );
	}
}