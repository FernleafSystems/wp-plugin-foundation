<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Configuration\Ops;

use FernleafSystems\Wordpress\Services\Services;
use FernleafSystems\Wordpress\Services\Utilities\File\Paths;

class Read {

	/**
	 * @throws \Exception
	 */
	public static function FromFile( string $path ) :array {
		$oFS = Services::WpFs();
		foreach ( [ 'json', 'php' ] as $ext ) {
			$sCfg = Paths::AddExt( $path, $ext );
			if ( $oFS->isFile( $sCfg ) ) {
				return self::FromString( $oFS->getFileContentUsingInclude( $sCfg ) );
			}
		}
		throw new \LogicException( 'No config file present for slug: '.basename( $path ) );
	}

	/**
	 * @throws \Exception
	 */
	public static function FromString( string $def ) :array {
		$spec = [];
		$def = trim( $def );

		if ( !empty( $def ) ) {
			if ( \strpos( $def, '{' ) !== 0 ) {
				throw new \Exception( 'Invalid plugin spec configuration.' );
			}
			$spec = \json_decode( $def, true );
		}

		if ( empty( $spec ) || !is_array( $spec ) ) {
			throw new \Exception( 'Could not parse the definition file.' );
		}

		return $spec;
	}
}