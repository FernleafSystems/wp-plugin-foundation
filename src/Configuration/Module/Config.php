<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Configuration\Module;

use FernleafSystems\Utilities\Data\Adapter\StdClassAdapter;
use FernleafSystems\Wordpress\Services\Services;

/**
 * @property array[] $admin_notices
 * @property mixed[] $definitions
 * @property array   $menu_items
 * @property array[] $options
 * @property array[] $sections
 * @property array   $properties
 * @property array   $requirements
 * @property string  $def_filehash
 * @property bool    $needs_save
 */
class Config {

	use StdClassAdapter;

	/**
	 * @var string
	 */
	protected $sSlug;

	/**
	 * @return $this
	 */
	public function cleanTransientStorage() {
		Services::WpGeneral()->deleteTransient( $this->getSpecTransientStorageKey() );
		return $this;
	}

	/**
	 * @return array
	 */
	public function getAdminNotices() {
		return is_array( $this->admin_notices ) ? $this->admin_notices : [];
	}

	/**
	 * @param string $key
	 * @return mixed|null
	 */
	public function getDef( string $key ) {
		return $this->definitions[ $key ] ?? null;
	}

	public function getOptionType( string $key ) :?string {
		return $this->getOptDefinition( $key )[ 'type' ] ?? null;
	}

	/**
	 * @return string
	 */
	public function getModuleSlug() {
		if ( !isset( $this->sSlug ) ) {
			$this->sSlug = $this->getProperty( 'slug' );
		}
		return $this->sSlug;
	}

	/**
	 * @return int
	 */
	public function getLoadPriority() {
		$nPriority = $this->getProperty( 'load_priority' );
		return is_null( $nPriority ) ? 100 : $nPriority;
	}

	/**
	 * @return int
	 */
	public function getMenuPriority() {
		$nPriority = $this->getProperty( 'menu_priority' );
		return is_null( $nPriority ) ? 100 : $nPriority;
	}

	/**
	 * @return string
	 */
	public function getProcessorActionHook() {
		return $this->getProperty( 'hook' );
	}

	/**
	 * @return string
	 */
	public function getStorageKey() {
		$key = $this->getProperty( 'storage_key' );
		if ( empty( $key ) ) {
			$key = $this->getModuleSlug();
		}
		return $key.'_options';
	}

	/**
	 * @return string
	 */
	public function getTagline() {
		return $this->getProperty( 'tagline' );
	}

	public function getWpCliCfg() :array {
		return array_merge(
			[
				'enabled' => true,
				'root'    => $this->getModuleSlug(),
			],
			$this->getRawData_FullFeatureConfig()[ 'wpcli' ] ?? []
		);
	}

	/**
	 * @return array
	 * @deprecated 0.8
	 */
	public function getRawData_MenuItems() {
		return $this->menu_items ?? [];
	}

	/**
	 * @return string
	 */
	public function getSpecTransientStorageKey() {
		return 'icwp_'.md5( $this->getModuleSlug() );
	}

	/**
	 * @return array
	 */
	public function getRawData_AllOptions() {
		return $this->options;
	}

	/**
	 * @return array
	 */
	public function getRawData_OptionsSections() {
		return $this->getRawData_FullFeatureConfig()[ 'sections' ] ?? [];
	}

	public function getOptDefinition( string $key ) :array {
//		return $this->options[ $key ] ?? [];
		return $this->getRawData_SingleOption( $key );
	}

	/**
	 * @param string $key
	 * @return array
	 */
	public function getRawData_SingleOption( $key ) {
		if ( is_array( $this->options ) ) {
			foreach ( $this->options as $option ) {
				if ( isset( $option[ 'key' ] ) && ( $key == $option[ 'key' ] ) ) {
					return $option;
				}
			}
		}
		return null;
	}

	/**
	 * @param string $topLevelKey
	 * @return array
	 */
	public function getRawData_FullFeatureConfig( $topLevelKey = '' ) {
		return empty( $topLevelKey ) ? $this->getRawDataAsArray() : $this->get( $topLevelKey );
	}

	/**
	 * @param string $sSlug
	 * @return array|null
	 */
	public function getSection( $sSlug ) {
		return $this->getSections()[ $sSlug ] ?? null;
	}

	/**
	 * @param bool $bIncludeHidden
	 * @return array[]
	 */
	public function getSections( $bIncludeHidden = false ) {
		$aSections = [];
		foreach ( $this->sections as $aSection ) {
			if ( $bIncludeHidden || !isset( $aSection[ 'hidden' ] ) || !$aSection[ 'hidden' ] ) {
				$aSections[ $aSection[ 'slug' ] ] = $aSection;
			}
		}
		return $aSections;
	}

	/**
	 * @param string $sSlug
	 * @return array
	 */
	public function getSection_Requirements( $sSlug ) {
		$aSection = $this->getSection( $sSlug );
		return array_merge(
			[ 'php_min' => '7.0' ],
			$aSection[ 'reqs' ] ?? []
		);
	}

	/**
	 * @return array
	 */
	public function getWizardDefinitions() {
		$aW = $this->getDef( 'wizards' );
		return is_array( $aW ) ? $aW : [];
	}

	/**
	 * @return bool
	 */
	public function hasWizard() {
		return count( $this->getWizardDefinitions() ) > 0;
	}

	/**
	 * @return bool
	 */
	public function hasCustomActions() {
		return (bool)$this->getProperty( 'has_custom_actions' );
	}

	/**
	 * @param string $sSectionSlug
	 * @return bool
	 */
	public function isSectionReqsMet( $sSectionSlug ) {
		$aReqs = $this->getSection_Requirements( $sSectionSlug );
		return Services::Data()->getPhpVersionIsAtLeast( $aReqs[ 'php_min' ] );
	}

	/**
	 * Base constructor.
	 * @param array $def
	 */
	public function __construct( array $def = [] ) {
		$this->applyFromArray( $def );
	}

	/**
	 * @return bool
	 */
	public function hasDefinition() {
		return !empty( $this->getRawDataAsArray() );
	}

	/**
	 * @param string $key
	 * @return mixed|null
	 */
	public function getProperty( $key ) {
		return $this->properties[ $key ] ?? null;
	}

	/**
	 * @param string $key
	 * @return null|array
	 */
	public function getRequirement( $key ) {
		return $this->requirements[ $key ] ?? null;
	}

	/**
	 * @param string $sParentCategory
	 * @param string $sKey
	 * @return null|mixed
	 */
	protected function get( $sParentCategory, $sKey = '' ) {
		if ( empty( $sKey ) ) {
			return $this->{$sParentCategory} ?? null;
		}
		return $this->{$sParentCategory}[ $sKey ] ?? null;
	}

	/**
	 * @param string
	 * @return null|array
	 * @deprecated 0.1.2
	 */
	public function getDefinition( $sDefKey ) {
		return $this->getDef( $sDefKey );
	}
}