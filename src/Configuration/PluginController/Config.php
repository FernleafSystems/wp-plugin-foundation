<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Configuration\PluginController;

/**
 * @property array $action_links
 * @property array $includes
 * @property array $labels
 * @property array $menu
 * @property array $paths
 * @property array $plugin_meta
 * @property array $plugin_modules
 * @property array $upgrades
 * @property array $update_first_detected
 */
class Config extends \FernleafSystems\Wordpress\Plugin\Foundation\Configuration\Module\Config {

	/**
	 * @param string $key
	 * @return null|string
	 */
	public function getActionLinks( $key ) {
		return $this->action_links[ $key ] ?? null;
	}

	/**
	 * @return string
	 */
	public function getBasePermissions() {
		return $this->getProperty( 'base_permissions' );
	}

	/**
	 * @param string $key
	 * @return null|array
	 */
	public function getInclude( $key ) {
		return $this->includes[ $key ] ?? null;
	}

	/**
	 * @return bool
	 */
	public function getIsWpmsNetworkAdminOnly() {
		return $this->getProperty( 'wpms_network_admin_only' );
	}

	/**
	 * @return array
	 */
	public function getLabels() :array {
		return is_array( $this->labels ) ? $this->labels : [];
	}

	/**
	 * @param string $sKey
	 * @return null|string
	 */
	public function getMenuSpec( $sKey ) {
		return $this->menu[ $sKey ] ?? null;
	}

	/**
	 * @return array
	 */
	public function getPluginModules() {
		$aModules = $this->plugin_modules;
		$aActiveModules = [];

		if ( !empty( $aModules ) && is_array( $aModules ) ) {
			foreach ( $aModules as $nPosition => $aFeature ) {
				if ( isset( $aFeature[ 'hidden' ] ) && $aFeature[ 'hidden' ] ) {
					continue;
				}
				$aActiveModules[ $aFeature[ 'slug' ] ] = $aFeature;
			}
		}
		return $aActiveModules;
	}

	/**
	 * @return string
	 */
	public function getParentSlug() {
		return $this->getProperty( 'slug_parent' );
	}

	/**
	 * @return string
	 */
	public function getPluginSlug() {
		return $this->getProperty( 'slug_plugin' );
	}

	/**
	 * @return string
	 */
	public function getPluginNamespace() {
		return $this->getProperty( 'namespace' );
	}

	/**
	 * @param string $sKey
	 * @return null|string
	 */
	public function getPath( $sKey ) {
		return $this->paths[ $sKey ] ?? null;
	}

	/**
	 * @return array
	 */
	public function getPluginMeta() {
		return is_array( $this->plugin_meta ) ? $this->plugin_meta : [];
	}

	/**
	 * @return string
	 */
	public function getTextDomain() {
		return $this->getProperty( 'text_domain' );
	}

	/**
	 * @param string $sVersion
	 * @return int
	 */
	public function getUpdateFirstDetected( string $sVersion ) :int {
		return $this->update_first_detected[ $sVersion ] ?? 0;
	}

	public function getUpgradeSettings() :array {
		return is_array( $this->get( 'upgrades' ) ) ? $this->get( 'upgrades' ) : [];
	}

	/**
	 * @param string $sVersion
	 * @param int    $nTime
	 * @return $this
	 */
	public function setUpdateFirstDetected( $sVersion, $nTime ) {
		if ( !is_array( $this->update_first_detected ) || count( $this->update_first_detected ) > 3 ) {
			$this->update_first_detected = [];
		}
		$a = $this->update_first_detected;
		$a[ $sVersion ] = $nTime;
		$this->update_first_detected = $a;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getVersion() {
		return $this->getProperty( 'version' );
	}
}