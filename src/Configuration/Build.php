<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Configuration;

use FernleafSystems\Wordpress\Services\Utilities\File\Paths;
use FernleafSystems\Wordpress\Plugin\Foundation\Configuration\{
	Module\Config,
	Ops\ReadDefinition,
	Ops\Save,
};
use FernleafSystems\Wordpress\Services\Services;

class Build {

	/**
	 * @throws \Exception
	 */
	public function build( string $pathRoot, string $storageKey, ?Config $cfg = null ) :Config {
		$sPath = $this->locateFullPath( $pathRoot );

		$reader = new ReadDefinition( $sPath, $storageKey );
		if ( empty( $cfg ) ) {
			$cfg = new Config();
		}
		$cfg = $cfg->applyFromArray( $reader->read() );

		if ( empty( $cfg->def_filehash ) ) {
			$cfg->def_filehash = hash_file( 'md5', $sPath );
			$cfg->needs_save = true;
		}
		elseif ( $cfg->def_filehash !== hash_file( 'md5', $sPath ) ) { // Need to rebuild
			$cfg->applyFromArray( $reader->fromFile() );
			$cfg->def_filehash = \hash_file( 'md5', $sPath );
			$cfg->needs_save = true;
		}
		Save::ToWp( $cfg, $storageKey );
		return $cfg;
	}

	private function locateFullPath( string $pathRoot ) :string {
		foreach ( [ 'json', 'php' ] as $ext ) {
			$cfg = Paths::AddExt( $pathRoot, $ext );
			if ( Services::WpFs()->isFile( $cfg ) ) {
				return $cfg;
			}
		}
		throw new \LogicException( 'No config file present for slug: '.\basename( $pathRoot ) );
	}
}