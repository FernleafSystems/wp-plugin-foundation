<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Wizard;

use FernleafSystems\Utilities\Response;
use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;
use FernleafSystems\Wordpress\Services\Services;

abstract class Base {

	use ModConsumer;

	/**
	 * @var WizardVo
	 */
	private $oWizard;

	public function __construct() {
	}

	public function init() {
		$mod = $this->getMod();
		add_action( $mod->prefix( 'no_nonce_query_action' ), [ $this, 'handleNoNonceQueryAction' ] );
		add_action( $mod->prefix( 'query_action' ), [ $this, 'handleQueryAction' ] );
		add_action( $mod->prefix( 'authenticated_query_action' ), [ $this, 'handleQueryActionAuthenticated' ] );
	}

	/**
	 * @param $action
	 */
	public function handleNoNonceQueryAction( $action ) {
		if ( $action == 'wizard' ) {
			$sWizard = Services::Request()->query( 'wizard', '' );
			if ( $this->isSupportedWizard( $sWizard ) ) {
				$this->wizardQueryAction( $sWizard, 'no_nonce_query_action' );
			}
		}
	}

	/**
	 * @param string $action
	 */
	public function handleQueryAction( $action ) {
		if ( $action == 'wizard' ) {
			$sWizard = Services::Request()->query( 'wizard', '' );
			if ( $this->isSupportedWizard( $sWizard ) ) {
				$this->wizardQueryAction( $sWizard, 'query_action' );
			}
		}
	}

	/**
	 * @param $action
	 */
	public function handleQueryActionAuthenticated( $action ) {
		if ( $action == 'wizard' ) {
			$sWizard = Services::Request()->query( 'wizard', '' );
			if ( $this->isSupportedWizard( $sWizard ) ) {
				$this->wizardQueryAction( $sWizard, 'authenticated_query_action' );
			}
		}
	}

	/**
	 * @param string $sWizard
	 * @param string $sHookType
	 */
	protected function wizardQueryAction( $sWizard, $sHookType ) {
		$oWizard = $this->loadWizardVo( $sWizard );
		if ( !is_null( $oWizard ) && $oWizard->getHookType() == $sHookType ) {
			$this->loadWizard( $oWizard );
		}
	}

	/**
	 * @param WizardVo $oWizard
	 * @throws \Exception
	 * @uses echo()
	 */
	protected function loadWizard( $oWizard ) {
		try {
			$sContent = $this->setCurrentWizard( $oWizard )
							 ->renderWizard();
		}
		catch ( \Exception $oE ) {
			$sContent = $oE->getMessage();
		}
		echo $sContent;
		die();
	}

	/**
	 * @return array
	 */
	protected function getWizards() {
		$wizards = $this->getMod()->getDef( 'wizards' );
		return ( is_array( $wizards ) && !empty( $wizards ) ) ? $wizards : [];
	}

	/**
	 * @param string $sSlug
	 * @return WizardVo|null
	 */
	public function loadWizardVo( $sSlug ) {
		$oWiz = null;
		$aWzs = $this->getWizards();
		if ( !empty( $aWzs[ $sSlug ] ) && is_array( $aWzs[ $sSlug ] ) ) {
			$oWiz = ( new WizardVo() )->applyFromArray( $aWzs[ $sSlug ] )
									  ->setSlug( $sSlug );
		}
		return $oWiz;
	}

	/**
	 * @param string $sSlug
	 * @return bool
	 */
	protected function isSupportedWizard( $sSlug ) {
		return in_array( $sSlug, array_keys( $this->getWizards() ) );
	}

	/**
	 * Ensure to only ever process supported wizards
	 */
	public function ajaxWizardRenderStep() {
		$oReq = Services::Request();

		$sWizard = $oReq->post( 'wizard_slug' );
		if ( $this->isSupportedWizard( $sWizard ) ) {

			$this->setCurrentWizard( $this->loadWizardVo( $sWizard ) ); //TODO
			$aNextStep = $this->getWizardNextStep( $oReq->post( 'wizard_steps' ), $oReq->post( 'current_index' ) );
			$this->getMod()
				 ->sendAjaxResponse(
					 true,
					 [ 'next_step' => $aNextStep ]
				 );
		}
	}

	public function ajaxWizardProcessStepSubmit() {
		$oResponse = $this->processWizardStep( Services::Request()->post( 'wizard-step' ) );
		if ( !empty( $oResponse ) ) {
			$this->sendWizardResponse( $oResponse );
		}
	}

	/**
	 * @param string $sStep
	 * @return \FernleafSystems\Utilities\Response|null
	 */
	protected function processWizardStep( $sStep ) {
		return null;
	}

	/**
	 * @param Response $oResponse
	 */
	protected function sendWizardResponse( $oResponse ) {

		$sMessage = $oResponse->getMessageText();
		if ( $oResponse->successful() ) {
			$sMessage .= '<br />'.sprintf( 'Please click %s to continue.', __( 'Next' ) );
		}
		else {
			$sMessage = sprintf( '%s: %s', __( 'Error' ), $sMessage );
		}

		$oResponse->setMessageText( $sMessage );

		Services::Respond()
				->setResponse( $oResponse )
				->send();
	}

	/**
	 * @return string
	 * @throws \Exception
	 */
	protected function renderWizard() {
		$sTemplate = sprintf( 'wizard/page_wizard_%s.twig', $this->getWizard()->isDynamic() ? 'dynamic' : 'standard' );
		return $this->getRender()
					->setTemplate( $sTemplate )
					->setRenderVars( $this->getDisplayData() )
					->setTemplateEngineTwig()
					->render();
	}

	/**
	 * @return array
	 */
	protected function getDisplayData() {
		$render = $this->getMod()->getModuleRender();
		$paths = $this->getCon()->getFoundationController()->getPluginPaths();
		$wizard = $this->getWizard();

		return [
			'strings' => [
				'page_title' => $this->getPageTitle()
			],
			'data'    => [
				'wizard_slug'       => $wizard->getSlug(),
				'wizard_steps'      => \json_encode( $this->determineWizardSteps() ),
				'wizard_first_step' => \json_encode( $this->getWizardFirstStep() ),
				'wizopts'           => \json_encode( $wizard->getWizardJsOptions() ),
			],
			'hrefs'   => [
				'form_action'      => Services::Request()->getUri(),
				'css_bootstrap'    => $paths->getPluginUrl_Css( 'bootstrap3.min.css' ),
				'css_pages'        => $paths->getPluginUrl_Css( 'pages.css' ),
				'css_steps'        => $paths->getPluginUrl_Css( 'jquery.steps.css' ),
				'css_fancybox'     => $paths->getPluginUrl_Css( 'jquery.fancybox.min.css' ),
				'css_globalplugin' => $paths->getPluginUrl_Css( 'global-plugin.css' ),
				'css_wizard'       => $paths->getPluginUrl_Css( 'wizard.css' ),
				'js_jquery'        => Services::Includes()->getUrl_Jquery(),
				'js_bootstrap'     => $paths->getPluginUrl_Js( 'bootstrap3.min.js' ),
				'js_fancybox'      => $paths->getPluginUrl_Js( 'jquery.fancybox.min.js' ),
				'js_globalplugin'  => $paths->getPluginUrl_Js( 'global-plugin.js' ),
				'js_steps'         => $paths->getPluginUrl_Js( 'jquery.steps.min.js' ),
				'js_wizard'        => $paths->getPluginUrl_Js( 'wizard.js' ),
				'plugin_banner'    => $paths->getPluginUrl_Image( 'pluginbanner_1500x500.png' ),
				'favicon'          => $paths->getPluginUrl_Image( 'pluginlogo_24x24.png' ),
			],
			'ajax'    => [
				'content'       => $render->getBaseAjaxActionRenderData( 'WizardProcessStepSubmit' ),
				'steps'         => $render->getBaseAjaxActionRenderData( 'WizardRenderStep' ),
				'steps_as_json' => $render->getBaseAjaxActionRenderData( 'WizardRenderStep', true ),
			],
			'flags'   => [
				'show_banner' => true
			]
		];
	}

	/**
	 * @return string
	 */
	protected function getPageTitle() {
		return sprintf( '%s Wizard', $this->getMod()->con()->getLabels()->getHumanName() );
	}

	/**
	 * @return string[]
	 */
	protected function determineWizardSteps() {
		// Special case: user doesn't meet even the basic plugin admin permissions
		if ( !$this->getCurrentUserCan() ) {
			return [ 'no_access' ];
		}
		return $this->buildSteps();
	}

	/**
	 * Overwrite this as required - should build an array of strings which represents the slugs of each
	 * wizard slide.
	 * @return array
	 */
	protected function buildSteps() {
		return array_keys( $this->getWizardStepsDefinition() );
	}

	/**
	 * @return bool
	 */
	protected function getCurrentUserCan() {
		$sPerms = $this->getWizard()->getMinUserPermissions();
		return empty( $sPerms ) || $sPerms == 'none'
			   || ( Services::WpUsers()->isUserLoggedIn() && !current_user_can( $sPerms ) );
	}

	/**
	 * @return array
	 */
	protected function getWizardFirstStep() {
		return $this->getWizardNextStep( $this->determineWizardSteps(), -1 );
	}

	/**
	 * @param array $aAllSteps
	 * @param int   $nCurrentStep
	 * @return array
	 */
	protected function getWizardNextStep( $aAllSteps, $nCurrentStep ) {

		$aStepData = [];

		// The assumption here is that the step data exists!

		try {

			if ( count( $aAllSteps ) < $nCurrentStep + 2 ) { // i.e. next step + array access starts at zero
				throw new \Exception( sprintf( 'There are not enough steps in the wizard to get step #%s', $nCurrentStep + 1 ) );
			}

			$aStepData = $this->getWizardStepsDefinition()[ $aAllSteps[ $nCurrentStep + 1 ] ];

			if ( empty( $aStepData[ 'slug' ] ) ) {
				throw new \Exception( 'Step slug data is empty.' );
			}

			$bRestrictedAccess = !isset( $aStepData[ 'restricted_access' ] ) || $aStepData[ 'restricted_access' ];
			if ( !$bRestrictedAccess || $this->getCurrentUserCan() ) {
				$aData = $this->getRenderDataForStep( $aStepData[ 'slug' ] );
				$aStepData[ 'content' ] = $this->renderWizardStep( $aStepData[ 'slug' ], $aData );
			}
			else {
				$aStepData[ 'content' ] = $this->renderSecurityAdminVerifyWizardStep( $nCurrentStep );
			}
		}
		catch ( \Exception $oE ) {
			$aStepData[ 'content' ] = 'Content could not be displayed due to error: '.$oE->getMessage();
		}

		return $aStepData;
	}

	/**
	 * @param int $nIndex
	 * @return string
	 * @throws \Exception
	 */
	protected function renderSecurityAdminVerifyWizardStep( $nIndex ) {
		return $this->renderWizardStep( 'admin_access_restriction_verify', [ 'current_index' => $nIndex ] );
	}

	/**
	 * @param string $sSlug
	 * @return array
	 */
	protected function getRenderDataForStep( $sSlug ) {
		$mod = $this->getMod();

		$aData = [
			'flags' => [
				'is_premium' => $mod->isPremium()
			],
			'hrefs' => [
				'dashboard' => $mod->getFeatureAdminPageUrl(),
				'gopro'     => 'http://icwp.io/ap',
			],
			'imgs'  => [],
		];

		$aAdd = [];
		switch ( $sSlug ) {
			case 'no_access':
				break;
			default:
				break;
		}

		return Services::DataManipulation()->mergeArraysRecursive( $aData, $aAdd );
	}

	/**
	 * @param string $sStepSlug
	 * @param array  $aRenderData
	 * @return string
	 * @throws \Exception
	 */
	protected function renderWizardStep( $sStepSlug, $aRenderData = [] ) {
		return $this->getRender()
					->setTemplate( sprintf( 'wizard/slide/%s/%s.twig', $this->getWizard()->getSlug(), $sStepSlug ) )
					->setRenderVars( $aRenderData )
					->render();
	}

	/**
	 * Overwrite to supply all the possible steps
	 * @return array[]
	 */
	protected function getAllDefinedSteps() {
		return $this->getWizard()->getSteps();
	}

	/**
	 * @return array[]
	 */
	private function getWizardStepsDefinition() {
		$aNoAccess = [
			'no_access' => [
				'title'             => 'No Access',
				'restricted_access' => false
			]
		];
		$aSteps = array_merge( $this->getAllDefinedSteps(), $aNoAccess );
		foreach ( $aSteps as $sSlug => $aStep ) {
			$aSteps[ $sSlug ][ 'slug' ] = $sSlug;
			$aSteps[ $sSlug ][ 'content' ] = '';
		}
		return $aSteps;
	}

	/**
	 * @return WizardVo
	 */
	public function getWizard() {
		return $this->oWizard;
	}

	/**
	 * @param WizardVo $oWiz
	 * @return $this
	 */
	public function setCurrentWizard( $oWiz ) {
		$this->oWizard = $oWiz;
		return $this;
	}
}