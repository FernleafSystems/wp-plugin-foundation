<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Utility;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;
use FernleafSystems\Wordpress\Services\Services;

class WorkingDir {

	use ModConsumer;

	/**
	 * @return string|false
	 */
	public function retrieve() {
		$cfg = $this->getCon()->config();
		$path = sprintf( '%s/%s/%s/',
			path_join( WP_CONTENT_DIR, $cfg->getParentSlug() ),
			$cfg->getPluginSlug(),
			$this->getMod()->getModSlug( false )
		);
		return Services::WpFs()->mkdir( $path ) ? realpath( $path ) : false;
	}
}