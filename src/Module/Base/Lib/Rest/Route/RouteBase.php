<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest\Route;

use FernleafSystems\Utilities\Data\Adapter\StdClassAdapter;
use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;
use FernleafSystems\Wordpress\Services\Services;
use FernleafSystems\Wordpress\Services\Utilities\File\Cache\CacheDefVO;

/**
 * @property bool $bypass_lock
 */
abstract class RouteBase extends \WP_REST_Controller {

	use Base\Traits\ModConsumer;
	use StdClassAdapter;

	/**
	 * @var bool
	 */
	private $isRegistered;

	/**
	 * https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/#examples
	 */
	public function register_routes() {
		if ( empty( $this->isRegistered ) ) {
			$this->isRegistered = true;
			if ( $this->isReady() ) {
				register_rest_route( $this->getNamespace(),
					$this->getRoutePath(),
					[ $this->buildRouteDefs() ]
				);
			}
		}
	}

	/**
	 * @return Base\Lib\Rest\Request\RequestVO
	 */
	public function getNewReqVO() {
		return new Base\Lib\Rest\Request\RequestVO();
	}

	/**
	 * @param array[] $args
	 * @return array[]
	 */
	protected function applyArgsDefaults( $args ) {
		return array_map(
			function ( $arg ) {

				$arg[ 'validate_callback' ] = function ( $value, $request, $reqArgKey ) {
					return $this->validateRequestArg( $value, $request, $reqArgKey );
				};

				$arg[ 'sanitize_callback' ] = function ( $value, $request, $reqArgKey ) {
					return $this->sanitizeRequestArg( $value, $request, $reqArgKey );
				};

				return $arg;
			},
			$args
		);
	}

	public function buildRouteDefs() :array {
		return [
			'methods'             => $this->getArgMethods(),
			'callback'            => function ( \WP_REST_Request $req ) {
				return $this->executeApiRequest( $req );
			},
			'permission_callback' => function ( \WP_REST_Request $req ) {
				return $this->verifyPermission( $req );
			},
			'args'                => $this->applyArgsDefaults( $this->getRouteArgs() ),
		];
	}

	public function getCacheHandler() :RouteCache {
		return new RouteCache( $this );
	}

	protected function getNamespace() :string {
		return sprintf( '%s/v%s', $this->getCon()->getPrefix()->getPluginPrefix(), $this->getVersion() );
	}

	protected function getVersion() :int {
		$version = (int)$this->getMod()
							 ->getModuleConfig()
							 ->getDef( 'rest_version' );
		return empty( $version ) ? 1 : $version;
	}

	abstract public function getRoutePath() :string;

	/**
	 * @return array[]
	 */
	protected function getRouteArgs() :array {
		return array_merge( $this->getRouteArgsDefaults(), $this->getRouteArgsCustom() );
	}

	/**
	 * @return array[]
	 */
	protected function getRouteArgsCustom() :array {
		return [];
	}

	/**
	 * @return array[][]
	 */
	protected function getRouteArgsDefaults() :array {
		return [];
	}

	protected function getRouteSlug() :string {
		try {
			return strtolower( ( new \ReflectionClass( $this ) )->getShortName() );
		}
		catch ( \ReflectionException $e ) {
			return substr( md5( get_class( $this ) ), 0, 6 );
		}
	}

	public function getArgMethods() :array {
		return [ \WP_REST_Server::READABLE ];
	}

	/**
	 * @return CacheDefVO
	 * @deprecated 0.5.3
	 */
	public function getCacheDefinition() {
		return $this->getCacheHandler()->getCacheDefinition();
	}

	/**
	 * @return string
	 * @throws \Exception
	 */
	public function getWorkingDir() :string {
		$base = $this->getMod()->getRestHandler()->getWorkingDir();
		if ( !empty( $base ) ) {
			$dir = path_join( $base, 'r-'.$this->getRouteSlug() );
			Services::WpFs()->mkdir( $dir );
			if ( !empty( realpath( $dir ) ) ) {
				return $dir;
			}
		}
		throw new \Exception( 'Working directory not available' );
	}

	protected function isReady() :bool {
		try {
			$bReady = $this->getWorkingDir() !== false;
		}
		catch ( \Exception $e ) {
			$bReady = false;
		}
		return $bReady;
	}

	protected function executeApiRequest( \WP_REST_Request $req ) :\WP_REST_Response {

		$responseData = $this->processRequest( $req );
		$responseData[ 'meta' ][ 'params' ] = $req->get_params();
		$responseData = $this->adjustApiResponse( $responseData, $req );

		$restResponse = new \WP_REST_Response();
		$restResponse->set_data( $responseData );

		if ( $responseData[ 'error' ] ?? false ) {
			$restResponse->set_status( empty( $responseData[ 'code' ] ) ? 500 : $responseData[ 'code' ] );
			$restResponse->header( 'Cache-Control', 'no-cache, must-revalidate' );
		}
		else {
			$restResponse->header( 'Cache-Control', 'public, max-age='.$this->getCacheHandler()->expiration );
			$restResponse->set_status( 200 );
		}

		return $restResponse;
	}

	protected function adjustApiResponse( array $response, \WP_REST_Request $req ) :array {
		return $response;
	}

	abstract protected function processRequest( \WP_REST_Request $req ) :array;

	/**
	 * @param string|mixed     $value
	 * @param \WP_REST_Request $request
	 * @param string           $reqArgKey
	 * @return \WP_Error|mixed
	 */
	public function sanitizeRequestArg( $value, $request, $reqArgKey ) {
		try {
			$value = rest_sanitize_request_arg( $value, $request, $reqArgKey );
			if ( !is_wp_error( $value ) ) {
				$value = $this->customSanitizeRequestArg( $value, $request, $reqArgKey );
			}
		}
		catch ( \Exception $e ) {
			$value = new \WP_Error( 400, $e->getMessage() );
		}
		return $value;
	}

	/**
	 * @param string|mixed     $value
	 * @param \WP_REST_Request $request
	 * @param string           $reqArgKey
	 * @return \WP_Error|bool
	 */
	public function validateRequestArg( $value, $request, $reqArgKey ) {
		try {
			$valid = rest_validate_request_arg( $value, $request, $reqArgKey );
			if ( $valid === true ) { // retain WP_ERROR info
				$valid = $this->customValidateRequestArg( $value, $request, $reqArgKey );
			}
		}
		catch ( \Exception $e ) {
			$valid = new \WP_Error( 400, $e->getMessage() );
		}
		return $valid;
	}

	/**
	 * @param mixed            $value
	 * @param \WP_REST_Request $request
	 * @param string           $reqArgKey
	 * @return \WP_Error|mixed
	 * @throws \Exception
	 */
	protected function customSanitizeRequestArg( $value, $request, $reqArgKey ) {
		return $value;
	}

	/**
	 * @param string|mixed     $value
	 * @param \WP_REST_Request $request
	 * @param string           $reqArgKey
	 * @return true
	 * @throws \Exception
	 */
	protected function customValidateRequestArg( $value, $request, $reqArgKey ) :bool {
		return true;
	}

	/**
	 * @return \WP_Error|bool
	 */
	protected function verifyPermission( \WP_REST_Request $req ) {
		return true;
	}
}