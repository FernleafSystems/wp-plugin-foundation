<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest\Route;

trait RestRouteConsumer {

	/**
	 * @var RouteBase|mixed
	 */
	private $oRoute;

	/**
	 * @return RouteBase|mixed
	 */
	public function getRestRoute() {
		return $this->oRoute;
	}

	/**
	 * @param RouteBase|mixed $oRR
	 * @return $this
	 */
	public function setRestRoute( $oRR ) :self {
		$this->oRoute = $oRR;
		return $this;
	}
}