<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest\Utility;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

class PreChecks {

	use Base\Traits\ModConsumer;

	/**
	 * @throws \Exception
	 */
	public function run() {
		try {
			$this->testFileSystem();
		}
		catch ( \Exception $oE ) {
			throw new \Exception( 'Service is temporarily unavailable. Report Code: '.$oE->getCode() );
		}
	}

	/**
	 * @throws \Exception
	 */
	private function testFileSystem() {
		$sDir = $this->getMod()->getWorkingDir();
		$nSpace = disk_free_space( $sDir );
		if ( $nSpace === false || $nSpace < 2000000 ) {
			throw new \Exception( 'Not enough disk space: '.$nSpace, 402 );
		}
	}
}