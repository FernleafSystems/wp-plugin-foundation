<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

use FernleafSystems\Wordpress\Services\Services;

class Render {

	use Traits\ModConsumer;

	public const DEFAULT_TEMPLATE_ADMIN_TWIG = 'admin/page/default.twig';

	public function __construct( ?Controller $mod = null ) {
		$this->setMod( $mod );
	}

	/**
	 * @return bool
	 */
	protected function canDisplayOptionsForm() {
		return $this->getCon()
					->getPermissions()
					->getHasPermissionToView();
	}

	/**
	 * Hooked to the menu item callback
	 */
	public function displayModuleAdminPage() {
		if ( $this->canDisplayOptionsForm() ) {
			$this->displayModulePage();
		}
		else {
			$this->displayRestrictedPage();
		}
	}

	/**
	 * Override this to customize anything with the display of the page
	 */
	protected function displayModulePage() {
		$this->display( [ 'content' => $this->getAllRenderedContentParts() ], '/admin/page/default' );
	}

	protected function displayRestrictedPage() {
		$this->display(
			[ 'flags' => [ 'show_summary' => false ] ],
			'/admin/page/restricted'
		);
	}

	/**
	 * @param array  $data
	 * @param string $template
	 */
	public function display( $data = [], $template = '' ) {
		echo $this->renderTemplate( $template, $data );
	}

	/**
	 * @return string
	 */
	protected function renderOptionsForm() {

		if ( $this->canDisplayOptionsForm() ) {
			$template = '/admin/options/options_form';
		}
		else {
			$template = 'admin/snippets/restricted';
		}

		return $this->getRender()
					->setTemplate( $template )
					->setRenderVars( $this->getBaseDisplayData() )
					->render();
	}

	/**
	 * The primary root is that for the plugin, with a fall-back root for the common foundation templates
	 * (mainly for twig use)
	 * @return \FernleafSystems\Wordpress\Services\Utilities\Render
	 */
	public function getRender() {
		$con = $this->getCon();
		$render = Services::Render()->setTemplateEngineTwig();
		$templatesDir = $con->getPluginPaths()->getPath_Templates();
		if ( Services::WpFs()->isDir( $templatesDir ) ) {
			$render->setTemplateRoot( $templatesDir );
		}
		$render->setTemplateRoot( $con->getFoundationController()->getPluginPaths()->getPath_Templates() );
		return $render;
	}

	/**
	 * @return array
	 */
	protected function getOptionsFormAjaxData() {
		return $this->getBaseAjaxActionRenderData( 'OptionsForm' );
	}

	/**
	 * @param string $action
	 * @param bool   $asJsonEncodedObject
	 * @return array
	 */
	public function getBaseAjaxActionRenderData( $action = '', $asJsonEncodedObject = false ) {
		$mod = $this->getMod();
		$aData = [
			'action'               => $mod->prefix( $action ), //wp ajax doesn't work without this.
			'module'               => $mod->getModSlug(),
			$mod->prefix( 'ajax' ) => 1, // a simple flag
			'ajaxurl'              => admin_url( 'admin-ajax.php' ),
		];
		$aData = $mod->getNoncer()->addNonceToArray( $aData, $action );
		return $asJsonEncodedObject ? json_encode( (object)$aData ) : $aData;
	}

	/**
	 * @param bool $bRenderEmbeddedContent - defaulting to false help prevent accidental infinite recursion
	 * @return array
	 */
	protected function getBaseDisplayData( $bRenderEmbeddedContent = false ) {
		$mod = $this->getMod();
		$modConfig = $mod->getModuleConfig();
		$labels = $mod->getCon()->getLabels();
		$oBuilderForUI = $mod->getBuilderForUI();

		$data = [
			'sPluginName'      => $labels->getHumanName(),
			'sFeatureName'     => $mod->getMainFeatureName(),
			'bFeatureEnabled'  => $mod->getIsMainFeatureEnabled(),
			'sTagline'         => $mod->getModuleConfig()->getTagline(),
			'sFeatureSlug'     => $mod->getModSlug(),
			'form_action'      => 'admin.php?page='.$mod->getModSlug(),
			'nOptionsPerRow'   => 1,
			'aPluginLabels'    => $labels->all(),
			'unique_render_id' => substr( md5( mt_rand() ), 0, 5 ),

			'bShowStateSummary' => false,
			'aSummaryData'      => apply_filters( $mod->prefix( 'get_feature_summary_data' ), [] ),

			'sPageTitle' => $mod->getMainFeatureName(),

			'data'    => [
				'nonce'             => $mod->getNoncer()->getNonceAsArray( 'form_options' ),
				'plugin'            => $mod->prefix(),
				'mod_slug'          => $mod->getModSlug(),
				'all_options'       => $oBuilderForUI->build(),
				'all_options_input' => $oBuilderForUI->collateAllFormInputsForAllOptions(),
				'forms'             => [
					'options' => $this->getOptionsFormAjaxData(),
				]
			],
			'strings' => $this->getDisplayStrings(),
			'flags'   => [
				'access_restricted'     => !$this->canDisplayOptionsForm(),
				'show_ads'              => $mod->getIsShowMarketing(),
				'show_summary'          => false,
				'wrap_page_content'     => true,
				'show_standard_options' => true,
				'show_content_actions'  => $modConfig->hasCustomActions(),
				'show_content_help'     => true,
				'show_alt_content'      => false,
				'can_wizard'            => $mod->canRunWizards(),
				'has_wizard'            => $modConfig->hasWizard(),
			],
			'hrefs'   => [
				'go_pro'         => 'http://icwp.io/shieldgoprofeature',
				'wizard_link'    => $mod->getUrl_WizardLanding(),
				'wizard_landing' => $mod->getUrl_WizardLanding(),
			],
			'content' => [
				'options_form'   => '',
				'alt'            => '',
				'actions'        => '',
				'help'           => '',
				'wizard_landing' => ''
			]
		];

		if ( $bRenderEmbeddedContent ) { // prevents recursive loops
			$data[ 'content' ] = $this->getAllRenderedContentParts();
			$data[ 'flags' ][ 'show_content_help' ] = strpos( $data[ 'content' ][ 'help' ], 'Error:' ) !== 0;
		}

		return $data;
	}

	/**
	 * @return array
	 */
	protected function getAllRenderedContentParts() {
		return [
			'options_form'   => $this->renderOptionsForm(),
			'alt'            => '',
			'actions'        => $this->getContentCustomActions(),
			'help'           => $this->getContentHelp(),
			'wizard_landing' => $this->getContentWizardLanding()
		];
	}

	/**
	 * @return array
	 */
	protected function getDisplayStrings() {
		$mod = $this->getMod();
		$cfg = $mod->getModuleConfig();
		return [
			'go_to_settings'    => __( 'Settings' ),
			'on'                => __( 'On' ),
			'off'               => __( 'Off' ),
			'more_info'         => __( 'More Info' ),
			'blog'              => __( 'Blog' ),
			'save_all_settings' => __( 'Save All Settings' ),
			'see_help_video'    => __( 'Watch Help Video' ),
			'btn_save'          => __( 'Save Options' ),
			'btn_options'       => __( 'Options' ),
			'btn_help'          => __( 'Help' ),
			'btn_actions'       => $cfg->hasCustomActions() ? __( 'Actions' ) : __( 'No Actions' ),
			'btn_wizards'       => $cfg->hasWizard() ? __( 'Wizards' ) : __( 'No Wizards' ),
			'options_title'     => __( 'Options' ),
			'options_summary'   => __( 'Configure Module' ),
			'actions_title'     => __( 'Actions and Info' ),
			'actions_summary'   => __( 'Perform actions for this module' ),
			'help'              => __( 'Help' ),
			'learn_more'        => __( 'Learn More' ),
		];
	}

	/**
	 * @return string
	 */
	protected function getContentCustomActions() {
		return '<h3 style="margin: 10px 0 100px">'.__( 'No Actions For This Module' ).'</h3>';
	}

	/**
	 * @return string
	 */
	protected function getContentHelp() {
		return 'todo: help content';
	}

	/**
	 * @return string
	 */
	protected function getContentWizardLanding() {
		$oModCon = $this->getMod();
		$oModConfig = $oModCon->getModuleConfig();
		$aData = $this->getBaseDisplayData();

		if ( $oModConfig->hasWizard() && $oModCon->canRunWizards() ) { //TODO: handler
			$aData[ 'content' ][ 'wizard_landing' ] = $this->getWizardHandler()->renderWizardLandingSnippet();
		}
		return $this->renderTemplate( 'snippets/module-wizard-template.php', $aData );
	}

	public function renderTemplate( string $template, array $data = [] ) :string {
		try {
			$out = $this
				->getRender()
				->setTemplate( $template )
				->setRenderVars(
					Services::DataManipulation()->mergeArraysRecursive( $this->getBaseDisplayData(), $data ) )
				->render();
		}
		catch ( \Exception $e ) {
			$out = $e->getMessage();
		}
		return $out;
	}
}