<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;
use FernleafSystems\Wordpress\Services\Services;

abstract class Notifications {

	use Base\Traits\ModConsumer;

	public function init() {
		add_filter( $this->getMod()->prefix( 'generate_admin_notices' ), [ $this, 'genNotices' ], 10, 2 );
	}

	/**
	 * @param array  $aCurrentNotices
	 * @param string $sType
	 * @return array
	 */
	public function genNotices( $aCurrentNotices, $sType ) {
		if ( !is_array( $aCurrentNotices ) ) {
			$aCurrentNotices = [];
		}

		if ( $this->isPermittedNoticeType( $sType ) ) {
			$aNotices = $this->getAdminNoticesOfType( $sType );
			foreach ( $aNotices as $sId => $aNotice ) {
				$aNotice[ 'id' ] = $sId;
				$sNoticeContent = $this->processNotice( $aNotice );
				if ( !empty( $sNoticeContent ) && is_string( $sNoticeContent ) ) {
					$aCurrentNotices[ $sId ] = $sNoticeContent;
				}
			}
		}
		else {
			trigger_error( 'Call genNotices() filter with invalid Type: '.$sType );
		}

		return $aCurrentNotices;
	}

	/**
	 * @param array $aNoticeData
	 * @return string
	 */
	abstract protected function processNotice( $aNoticeData );

	/**
	 * @param array $aData
	 * @return string
	 * @throws \Exception
	 */
	protected function renderNotice( array $aData ) {
		if ( empty( $aData[ 'notice_attributes' ] ) ) {
			throw new \Exception( 'notice_attributes is empty' );
		}

		if ( !isset( $aData[ 'icwp_ajax_nonce' ] ) ) {
			$aData[ 'icwp_ajax_nonce' ] = wp_create_nonce( 'icwp_ajax' );
		}
		if ( !isset( $aData[ 'icwp_admin_notice_template' ] ) ) {
			$aData[ 'icwp_admin_notice_template' ] = $aData[ 'notice_attributes' ][ 'id' ];
		}

		if ( !isset( $aData[ 'notice_classes' ] ) ) {
			$aData[ 'notice_classes' ] = [];
		}
		if ( is_array( $aData[ 'notice_classes' ] ) ) {
			if ( empty( $aData[ 'notice_classes' ] ) ) {
				$aData[ 'notice_classes' ][] = 'updated';
			}
			$aData[ 'notice_classes' ][] = $aData[ 'notice_attributes' ][ 'type' ];
		}
		$aData[ 'notice_classes' ] = implode( ' ', $aData[ 'notice_classes' ] );

		return $this->getRender()
					->setTemplate( 'notices/'.$aData[ 'notice_attributes' ][ 'id' ] )
					->setRenderVars( $aData )
					->render();
	}

	/**
	 * @param array $aNoticeAttributes
	 * @return bool
	 */
	protected function getIfDisplayAdminNotice( $aNoticeAttributes ) {
		$oWpNotices = Services::WpAdminNotices();

		if ( empty( $aNoticeAttributes[ 'schedule' ] ) || !in_array( $aNoticeAttributes[ 'schedule' ], [
				'once',
				'conditions',
				'version'
			] ) ) {
			$aNoticeAttributes[ 'schedule' ] = 'conditions';
		}

		if ( $aNoticeAttributes[ 'schedule' ] == 'once'
			 && ( !Services::WpUsers()
						   ->canSaveMeta() || $oWpNotices->getAdminNoticeIsDismissed( $aNoticeAttributes[ 'id' ] ) )
		) {
			return false;
		}

		if ( $aNoticeAttributes[ 'schedule' ] == 'version' && ( $this->getMod()
																	 ->getVersion() == $oWpNotices->getAdminNoticeMeta( $aNoticeAttributes[ 'id' ] ) ) ) {
			return false;
		}

		if ( isset( $aNoticeAttributes[ 'type' ] ) && $aNoticeAttributes[ 'type' ] == 'promo' ) {
			if ( $this->nPromoNoticesCount > 0 || Services::WpGeneral()->isMobile() ) {
				return false;
			}
			$this->nPromoNoticesCount++; // we limit the number of promos displayed at any time to 1
		}

		return true;
	}

	/**
	 * @return array
	 */
	protected function getAdminNotices() {
		return $this->getMod()->getModuleConfig()->getAdminNotices();
	}

	/**
	 * @param string $sType
	 * @return array
	 */
	protected function getAdminNoticesOfType( $sType ) {
		$aNotices = [];
		foreach ( $this->getAdminNotices() as $sId => $aNotice ) {
			if ( $aNotice[ 'type' ] == $sType ) {
				$aNotices[ $sId ] = $aNotice;
			}
		}
		return $aNotices;
	}

	/**
	 * @return array
	 */
	protected function getPermittedNoticeTypes() {
		return [ 'success', 'warning', 'danger', 'info' ];
	}

	/**
	 * @param string $sType
	 * @return bool
	 */
	protected function isPermittedNoticeType( $sType ) {
		return in_array( $sType, $this->getPermittedNoticeTypes() );
	}
}