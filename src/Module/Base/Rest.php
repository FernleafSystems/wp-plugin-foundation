<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Rest\Route\RouteBase;
use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;

abstract class Rest extends \FernleafSystems\Wordpress\Plugin\Core\Rest\RestHandler {

	use ModConsumer;

	/**
	 * @return Lib\Rest\Route\RouteBase[]
	 */
	public function buildRoutes() :array {
		/** @var RouteBase[] $routes */
		$routes = parent::buildRoutes();
		return array_map( fn( $route ) => $route->setMod( $this->getMod() ), $routes );
	}
}