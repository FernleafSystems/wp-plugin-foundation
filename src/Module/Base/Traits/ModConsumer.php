<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits;

use FernleafSystems\Wordpress\Plugin\Foundation;

trait ModConsumer {

	/**
	 * @var Foundation\Module\Base\Controller
	 */
	private $oModCon;

	/**
	 * @return Foundation\Module\Base\Controller
	 */
	public function getMod() {
		return $this->oModCon;
	}

	/**
	 * @return Foundation\Module\Base\Controller
	 * @deprecated 0.1.2
	 */
	public function getModuleController() {
		return $this->getMod();
	}

	/**
	 * @return Foundation\Module\Base\Options
	 */
	public function getOptions() {
		return $this->getMod()->getOptions();
	}

	/**
	 * @return Foundation\Control\Controller
	 */
	public function getCon() {
		return $this->getMod()->getCon();
	}

	/**
	 * @return Foundation\Permissions\Permissions
	 */
	public function getPerms() {
		return $this->getCon()->getPermissions();
	}

	/**
	 * TODO: move to plugin controller as with Shield
	 * @return \FernleafSystems\Wordpress\Services\Utilities\Render
	 */
	public function getRender() {
		return $this->getMod()->getModuleRender()->getRender();
	}

	/**
	 * @param Foundation\Module\Base\Controller $oMod
	 * @return $this
	 */
	public function setMod( $oMod ) {
		$this->oModCon = $oMod;
		return $this;
	}

	/**
	 * @return Foundation\Control\Controller
	 * @deprecated 0.1.2
	 */
	public function getPluginController() {
		return $this->getCon();
	}

	/**
	 * @param Foundation\Module\Base\Controller $oMod
	 * @return $this
	 * @deprecated 0.1.2
	 */
	public function setModuleController( $oMod ) {
		return $this->setMod( $oMod );
	}
}