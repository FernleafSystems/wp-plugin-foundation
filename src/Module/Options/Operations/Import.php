<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Options\Operations;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Options;

class Import {

	/**
	 * @param Options $oOptions
	 * @param array   $aOptionsToImport
	 * @return bool
	 */
	public function fromArray( $oOptions, $aOptionsToImport ) {
		if ( !empty( $aOptionsToImport ) && is_array( $aOptionsToImport ) ) {
			$oOptions->setMultipleOptions( $aOptionsToImport );
			return true;
		}
		return false;
	}
}