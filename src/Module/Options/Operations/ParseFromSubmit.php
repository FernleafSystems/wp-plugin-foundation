<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Options\Operations;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Options;
use FernleafSystems\Wordpress\Plugin\Foundation\Utility\Prefix;
use FernleafSystems\Wordpress\Services\Services;

class ParseFromSubmit {

	/**
	 * @var string
	 */
	const CollateSeparator = '--SEP--';

	/**
	 * @param Options $oOptions
	 * @param string  $sAllOptionsInput - CollateSeparator separated list of all the input keys to be processed from
	 *                                  the $_POST
	 */
	public function parseAndStore( $oOptions, $sAllOptionsInput ) {
		if ( empty( $sAllOptionsInput ) ) {
			return;
		}
		$oDp = Services::Data();

		$aAllInputOptions = explode( self::CollateSeparator, $sAllOptionsInput );
		foreach ( $aAllInputOptions as $sInputKey ) {
			$aInput = explode( ':', $sInputKey );
			[ $sOptionType, $sOptionKey ] = $aInput;

			$sOptionValue = Services::Request()->post( $sOptionKey );
			if ( is_null( $sOptionValue ) ) {

				if ( $sOptionType == 'text' || $sOptionType == 'email' ) { //if it was a text box, and it's null, don't update anything
					continue;
				}
				elseif ( $sOptionType == 'checkbox' ) { //if it was a checkbox, and it's null, it means 'N'
					$sOptionValue = 'N';
				}
				elseif ( $sOptionType == 'integer' ) { //if it was a integer, and it's null, it means '0'
					$sOptionValue = 0;
				}
			}
			else { //handle any pre-processing we need to.

				if ( $sOptionType == 'text' || $sOptionType == 'email' ) {
					$sOptionValue = trim( $sOptionValue );
				}
				if ( $sOptionType == 'integer' ) {
					$sOptionValue = intval( $sOptionValue );
				}
				elseif ( $sOptionType == 'password' ) { //md5 any password fields
					$sTempValue = trim( $sOptionValue );
					if ( empty( $sTempValue ) ) {
						continue;
					}
					$sOptionValue = md5( $sTempValue );
				}
				elseif ( $sOptionType == 'array' ) { //arrays are textareas, where each is separated by newline
					$sOptionValue = array_filter( explode( "\n", esc_textarea( $sOptionValue ) ), 'trim' );
				}
				elseif ( $sOptionType == 'yubikey_unique_keys' ) { //ip addresses are textareas, where each is separated by newline and are 12 chars long
					$sOptionValue = $oDp->CleanYubikeyUniqueKeys( $sOptionValue );
				}
				elseif ( $sOptionType == 'email' && function_exists( 'is_email' ) && !is_email( $sOptionValue ) ) {
					$sOptionValue = '';
				}
				elseif ( $sOptionType == 'comma_separated_lists' ) {
					$sOptionValue = $oDp->extractCommaSeparatedList( $sOptionValue );
				}
				elseif ( $sOptionType == 'multiple_select' ) {
				}
			}

			if ( $sOptionType != 'noneditable_text' ) {
				$oOptions->setOpt( $sOptionKey, $sOptionValue );
			}
		}
	}
}