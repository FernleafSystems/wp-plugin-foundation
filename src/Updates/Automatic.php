<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Updates;

use FernleafSystems\Wordpress\Plugin\Foundation\Configuration\PluginController\ConfigConsumer;
use FernleafSystems\Wordpress\Plugin\Foundation\Root\File;
use FernleafSystems\Wordpress\Services\Services;

class Automatic {

	use ConfigConsumer;

	private File $rootFile;

	/**
	 * @param File $root
	 */
	public function __construct( File $root ) {
		$this->rootFile = $root;
		add_filter( 'auto_update_plugin', [ $this, 'onWpAutoUpdate' ], 10001, 2 );
		add_filter( 'set_site_transient_update_plugins', [ $this, 'setUpdateFirstDetectedAt' ] );
	}

	/**
	 * This will hook into the saving of plugin update information and if there is an update for this plugin, it'll add
	 * a data stamp to state when the update was first detected.
	 *
	 * @param \stdClass $updateData
	 * @return \stdClass
	 */
	public function setUpdateFirstDetectedAt( $updateData ) {
		$root = $this->rootFile ?? $this->oRootFile;

		if ( !empty( $updateData ) && !empty( $updateData->response )
			 && isset( $updateData->response[ $root->getPluginBaseFile() ] ) ) {
			// i.e. there's an update available
			$newVersion = Services::WpPlugins()->getUpdateNewVersion( $root->getPluginBaseFile() );
			if ( !empty( $newVersion ) ) {
				$this->getConfig()->setUpdateFirstDetected( $newVersion, Services::Request()->ts() );
			}
		}
		return $updateData;
	}

	/**
	 * This is a filter method designed to say whether WordPress plugin upgrades should be permitted,
	 * based on the plugin settings.
	 *
	 * @param bool          $doAutoUpdate
	 * @param string|object $mItemToUpdate
	 * @return bool
	 */
	public function onWpAutoUpdate( $doAutoUpdate, $mItemToUpdate ) {

		if ( is_object( $mItemToUpdate ) && !empty( $mItemToUpdate->plugin ) ) { // 3.8.2+
			$itemFile = $mItemToUpdate->plugin;
		}
		elseif ( is_string( $mItemToUpdate ) && !empty( $mItemToUpdate ) ) { //pre-3.8.2
			$itemFile = $mItemToUpdate;
		}
		else {
			// at this point we don't have a slug/file to use so we just return the current update setting
			return $doAutoUpdate;
		}

		// The item in question is this plugin...
		if ( $itemFile === ( $this->rootFile ?? $this->oRootFile )->getPluginBaseFile() ) {
			$autoupdateSpec = $this->getConfig()->getProperty( 'autoupdate' );

			$WP = Services::WpGeneral();
			if ( !$WP->isRunningAutomaticUpdates() && $autoupdateSpec == 'confidence' ) {
				$autoupdateSpec = 'yes';
			}

			switch ( $autoupdateSpec ) {

				case 'yes' :
					$doAutoUpdate = true;
					break;

				case 'block' :
					$doAutoUpdate = false;
					break;

				case 'confidence' :
					$doAutoUpdate = false;
					$sNewVersion = Services::WpPlugins()->getUpdateNewVersion( $itemFile );
					if ( !empty( $sNewVersion ) ) {
						$nFirstDetected = $this->getConfig()->getUpdateFirstDetected( $sNewVersion );
						$nTimeUpdateAvailable = Services::Request()->ts() - $nFirstDetected;
						$doAutoUpdate = ( $nFirstDetected > 0 && ( $nTimeUpdateAvailable > DAY_IN_SECONDS*2 ) );
					}
					break;

				case 'pass' :
				default:
					break;
			}
		}
		return $doAutoUpdate;
	}
}