<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Locale;

use FernleafSystems\Wordpress\Plugin\Foundation;

class TextDomain {

	use Foundation\Configuration\PluginController\ConfigConsumer;

	/**
	 * @param Foundation\Paths\Derived $oPluginPaths
	 * @return bool
	 */
	public function loadTextDomain( $oPluginPaths ) {
		return load_plugin_textdomain(
			$this->getConfig()->getTextDomain(),
			false,
			plugin_basename( $oPluginPaths->getPath_Languages() )
		);
	}
}