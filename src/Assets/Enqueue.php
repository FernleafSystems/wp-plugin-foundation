<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Assets;

use FernleafSystems\Wordpress\Plugin\Foundation;
use FernleafSystems\Wordpress\Services\Services;

class Enqueue {

	use Foundation\Configuration\PluginController\ConfigConsumer;

	public const ASSET_TYPE_CSS = 'css';
	public const ASSET_TYPE_JS = 'js';

	/**
	 * @var string - unique string to separate our enqueues from everyone else's
	 */
	private static string $sPrefix = '';

	/**
	 * @var array[] - the complete population of assets available to our plugins for enqueue
	 */
	private static $aAssetKeys;

	/**
	 * @var array[] - the complete population of assets available to our plugins for enqueue
	 */
	private static $aAdhocKeys;

	protected Foundation\Utility\Prefix $prefix;

	protected Foundation\Paths\Derived $paths;

	public function __construct( Foundation\Utility\Prefix $prefix, Foundation\Paths\Derived $paths ) {
		$this->prefix = $prefix;
		$this->paths = $paths;

		if ( empty( self::$sPrefix ) ) {
			self::$sPrefix = 'odp-foundation-';
		}

		$WP = Services::WpGeneral();
		if ( !$WP->isAjax() && !$WP->isCron() ) {
			// key to sharing assets between plugins and using foundation classes' assets instead of individual assets
			add_action( self::$sPrefix.'register_assets', [ $this, 'registerAssets' ] );
			add_action( 'wp_enqueue_scripts', [ $this, 'onWpEnqueueAssets' ], 1000 );
			add_action( 'admin_enqueue_scripts', [ $this, 'onWpEnqueueAssets' ], 1000 );
		}
	}

	/**
	 * @param string $sAssetKey
	 * @param string $sAssetType
	 */
	public static function AdHocEnqueue( $sAssetKey, $sAssetType = 'js' ) {
		if ( !is_array( self::$aAdhocKeys ) ) {
			self::$aAdhocKeys = [
				self::ASSET_TYPE_CSS => [],
				self::ASSET_TYPE_JS  => [],
			];
		}
		self::$aAdhocKeys[ $sAssetType ][] = $sAssetKey;
	}

	/**
	 * Registers all assets in the plugin with the global population of assets (if not already included)
	 * This allows us to easily share assets amongst plugins and especially to use the Foundation Classes
	 * plugin to cater for most shared assets.
	 */
	public function registerAssets() {
		$this->registerAssetsType( self::ASSET_TYPE_CSS )
			 ->registerAssetsType( self::ASSET_TYPE_JS );
	}

	/**
	 * @param string $sType
	 * @return $this
	 */
	private function registerAssetsType( $sType ) {

		$oSpec = $this->getConfig();
		$sVersion = $oSpec->getVersion();
		$aIncs = $oSpec->getInclude( 'register' );

		$aAssetKeys = $this->getAssetKeys();

		if ( isset( $aIncs[ $sType ] ) && is_array( $aIncs[ $sType ] ) ) {

			foreach ( $aIncs[ $sType ] as $sKey => $aDeps ) {

				if ( !in_array( $sKey, $aAssetKeys[ $sType ] ) ) {

					if ( $sType === self::ASSET_TYPE_CSS ) {

						$bReg = wp_register_style(
							self::$sPrefix.$sKey,
							$this->paths->getPluginUrl_Css( $sKey.'.'.$sType ),
							$this->prefixKeys( $aDeps ),
							$sVersion
						);
					}
					else {
						$bReg = wp_register_script(
							self::$sPrefix.$sKey,
							$this->paths->getPluginUrl_Js( $sKey.'.'.$sType ),
							$this->prefixKeys( $aDeps ),
							$sVersion
						);
					}

					if ( $bReg ) {
						$aAssetKeys[ $sType ][] = self::$sPrefix.$sKey;
					}
				}
			}
		}

		self::$aAssetKeys = $aAssetKeys;
		return $this;
	}

	/**
	 * @param string[] $aKeys
	 * @return string[]
	 */
	private function prefixKeys( $aKeys ) {
		return \array_map( function ( $sDep ) {
			if ( \str_starts_with( $sDep, 'wp-' ) ) {
				return \str_replace( 'wp-', '', $sDep );
			}
			else {
				return self::$sPrefix.$sDep;
			}
		}, $aKeys ); //prefix dependency keys
	}

	/**
	 * @return array[]
	 */
	private function getAssetKeys() {
		if ( !isset( self::$aAssetKeys ) ) {
			self::$aAssetKeys = [
				self::ASSET_TYPE_CSS => [],
				self::ASSET_TYPE_JS  => [],
			];
		}
		return self::$aAssetKeys;
	}

	public function onWpEnqueueAssets() {

		do_action( self::$sPrefix.'register_assets' );

		if ( current_action() == 'admin_enqueue_scripts' ) {
			$this->enqueueAdminAssets( self::ASSET_TYPE_JS );
			$this->enqueueAdminAssets( self::ASSET_TYPE_CSS );
		}
		else {
			$this->onWpEnqueueFrontendCss();
		}
	}

	public function onWpEnqueueFrontendCss() {
		$css = [];
		$inc = $this->getConfig()->getInclude( 'frontend' );
		if ( \is_array( $inc[ 'css' ] ) ) {
			$css = $inc[ 'css' ];
		}
		$this->processCssEnqueues( $css );
	}

	/**
	 * @param string $sAssetType - one of self::ASSET_TYPE_CSS self::ASSET_TYPE_JS
	 */
	protected function enqueueAdminAssets( $sAssetType ) {
		$assets = is_array( self::$aAdhocKeys ) ? self::$aAdhocKeys[ $sAssetType ] : [];

		$inc = $this->getConfig()->getInclude( 'admin' );
		if ( isset( $inc[ $sAssetType ] ) && is_array( $inc[ $sAssetType ] ) ) {
			$assets = \array_merge( $assets, $inc[ $sAssetType ] );
		}

		// it's the plugin admin page
		$page = Services::Request()->query( 'page' );
		if ( !empty( $page ) && \str_starts_with( $page, $this->prefix->prefix() ) ) {
			$inc = $this->getConfig()->getInclude( 'plugin_admin' );
			if ( isset( $inc[ $sAssetType ] ) && is_array( $inc[ $sAssetType ] ) ) {
				$assets = \array_merge( $assets, $inc[ $sAssetType ] );
			}
		}

		if ( $sAssetType == self::ASSET_TYPE_CSS ) {
			\array_map( 'wp_enqueue_style', $this->prefixKeys( $assets ) );
		}
		else {
			\array_map( 'wp_enqueue_script', $this->prefixKeys( $assets ) );
		}
	}

	/**
	 * @param array $aAssets
	 */
	private function processJsEnqueues( $aAssets ) {
		foreach ( $aAssets as $sAssetKey ) {
			wp_enqueue_script( self::$sPrefix.$sAssetKey );
		}
	}

	/**
	 * @param array $aAssets
	 */
	protected function processCssEnqueues( $aAssets ) {
		foreach ( $aAssets as $sAssetKey ) {
			wp_enqueue_style( self::$sPrefix.$sAssetKey );
		}
	}
}
