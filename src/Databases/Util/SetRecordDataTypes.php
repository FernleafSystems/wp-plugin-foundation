<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Databases\Util;

use FernleafSystems\Wordpress\Plugin\Foundation\Databases\Base\EntryVO;

class SetRecordDataTypes {

	public function setForRecord( $VO, array $colDefs ) {
		foreach ( $VO->getRawDataAsArray() as $key => $val ) {
			if ( isset( $colDefs[ $key ] ) ) {
				switch ( $colDefs[ $key ] ) {
					case 'int':
					case 'tinyint':
					case 'smallint':
					case 'mediumint':
					case 'bigint':
						$VO->{$key} = (int)$val;
						break;

					case 'bool':
					case 'boolean':
						$VO->{$key} = (bool)$val;
						break;

					case 'char':
					case 'varchar':
					case 'text':
						$VO->{$key} = (string)$val;
						break;

					default:
						break;
				}
			}
		}
	}

	/**
	 * @param EntryVO[] $VOs
	 * @param string[]  $colDefs
	 */
	public function setForRecords( array $VOs, array $colDefs = [] ) {
		array_map( function ( $VO ) use ( $colDefs ) {
			$this->setForRecord( $VO, $colDefs );
		}, $VOs );
	}
}