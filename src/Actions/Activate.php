<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Actions;

use FernleafSystems\Wordpress\Plugin\Foundation\Utility\Prefix;

class Activate extends Base {

	const SLUG = 'plugin_activate';

	public function __construct( Prefix $oPrefix ) {
		parent::__construct( $oPrefix->prefix( self::SLUG ) );
	}
}
