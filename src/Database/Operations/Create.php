<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations;

use FernleafSystems\Wordpress\Services\Services;

class Create extends Base {

	/**
	 * @param string $sCreateSql
	 * @return array
	 */
	public function create( $sCreateSql ) {
		return Services::WpDb()->dbDelta( $sCreateSql );
	}

	/**
	 * @return bool
	 */
	public function recreate() {
		$oDelete = new Delete( $this->getDbVo() );
		return $oDelete->drop();
	}
}