<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations;

use FernleafSystems\Wordpress\Services\Services;

class Verify extends Base {

	public function exists() :bool {
		return Services::WpDb()->tableExists( $this->getDbVo()->getTableName() );
	}

	/**
	 * TODO: Verify column properties
	 * @return bool
	 */
	public function valid() {
		$bValid = false;
		$aColumnsByDefinition = $this->getDbVo()->getTableColumnsByDefinition();
		if ( !empty( $aColumnsByDefinition ) && is_array( $aColumnsByDefinition ) ) {
			$aColumnsByDefinition = array_map( 'strtolower', $this->getDbVo()->getTableColumnsByDefinition() );
			$aActualColumns = Services::WpDb()->getColumnsForTable( $this->getDbVo()->getTableName(), 'strtolower' );
			$bValid = ( count( array_diff( $aActualColumns, $aColumnsByDefinition ) ) <= 0
						&& ( count( array_diff( $aColumnsByDefinition, $aActualColumns ) ) <= 0 ) );
		}
		return $bValid;
	}

	/**
	 * @param string $sColumnName
	 * @return bool
	 */
	public function hasColumn( $sColumnName ) {
		$aColumnsByDefinition = array_map( 'strtolower', $this->getDbVo()->getTableColumnsByDefinition() );
		return in_array( strtolower( $sColumnName ), $aColumnsByDefinition );
	}

	/**
	 * @param array $aColumnsSelected
	 * @return array
	 */
	public function verifyColumnsSelection( $aColumnsSelected ) {
		if ( !empty( $aColumns ) && is_array( $aColumns ) ) {
			$aColumns = array_intersect( $this->getDbVo()->getTableColumnsByDefinition(), $aColumnsSelected );
		}
		else {
			$aColumns = [];
		}
		return $aColumns;
	}
}