<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Permissions;

use FernleafSystems\Wordpress\Plugin\Foundation;

class Permissions {

	use Foundation\Configuration\PluginController\ConfigConsumer;

	/**
	 * @var boolean
	 */
	protected $bMeetsBasePermissions = false;

	/**
	 * @var Foundation\Utility\Prefix
	 */
	protected $oPrefix;

	/**
	 * @param Foundation\Utility\Prefix $oPrefix
	 */
	public function __construct( $oPrefix ) {
		$this->oPrefix = $oPrefix;
		add_action( 'init', [ $this, 'onWpInit' ], 0 );
	}

	/**
	 * v5.4.1: Nasty looping bug in here where this function was called within the 'user_has_cap' filter
	 * so we removed the "current_user_can()" or any such sub-call within this function
	 * @return bool
	 */
	public function getHasPermissionToManage() :bool {
		if ( apply_filters( $this->oPrefix->prefix( 'bypass_permission_to_manage' ), false ) ) {
			return true;
		}
		return $this->getMeetsBasePermissions()
			   && apply_filters( $this->oPrefix->prefix( 'has_permission_to_manage' ), true );
	}

	public function getHasPermissionToView() :bool {
		return $this->getHasPermissionToManage(); // TODO: separate view vs manage
	}

	/**
	 * Must be simple and cannot contain anything that would call filter "user_has_cap", e.g. current_user_can()
	 * @return bool
	 */
	public function getMeetsBasePermissions() {
		return $this->bMeetsBasePermissions;
	}

	public function onWpInit() {
		$this->bMeetsBasePermissions = current_user_can( $this->getConfig()->getBasePermissions() );
	}
}
