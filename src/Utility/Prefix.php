<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Utility;

use FernleafSystems\Wordpress\Plugin\Foundation;

class Prefix {

	use Foundation\Configuration\PluginController\ConfigConsumer;

	/**
	 * @param string $suffix
	 */
	public function prefixOption( $suffix = '' ) :string {
		return $this->prefix( $suffix, '_' );
	}

	/**
	 * @param string $suffix
	 * @param string $glue
	 */
	public function prefix( $suffix = '', $glue = '-' ) :string {
		$sPrefix = $this->getPluginPrefix( $glue );

		if ( $suffix == $sPrefix || strpos( $suffix, $sPrefix.$glue ) === 0 ) { //it already has the full prefix
			return $suffix;
		}
		return sprintf( '%s%s%s', $sPrefix, empty( $suffix ) ? '' : $glue, empty( $suffix ) ? '' : $suffix );
	}

	public function getOptionStoragePrefix() :string {
		return $this->getPluginPrefix( '_' ).'_';
	}

	/**
	 * @param string
	 */
	public function getPluginPrefix( $glue = '-' ) :string {
		return sprintf( '%s%s%s', $this->getConfig()->getParentSlug(), $glue, $this->getConfig()->getPluginSlug() );
	}
}
