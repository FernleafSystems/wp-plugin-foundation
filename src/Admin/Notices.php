<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Admin;

use FernleafSystems\Wordpress\Plugin\Foundation\Utility\PrefixConsumer;
use FernleafSystems\Wordpress\Services\Services;

class Notices {

	use PrefixConsumer;

	/**
	 * @var string
	 */
	protected $sFlashMessage;

	public function __construct() {
		add_action( 'wp', [ $this, 'flushFlashMessage' ] );
		add_action( 'admin_notices', [ $this, 'onWpAdminNotices' ] );
		add_action( 'network_admin_notices', [ $this, 'onWpAdminNotices' ] );
	}

	/**
	 * @param $msg
	 */
	public function addFlashMessage( $msg ) {
		Services::Response()->cookieSet( $this->getPrefix()->prefix( 'flash' ), esc_attr( $msg ) );
	}

	public function flushFlashMessage() {
		$cookie = $this->getPrefix()->prefix( 'flash' );
		$this->sFlashMessage = Services::Request()->cookie( $cookie, '' );
		if ( !empty( $this->sFlashMessage ) ) {
			$this->sFlashMessage = sanitize_text_field( $this->sFlashMessage );
		}
		Services::Response()->cookieDelete( $cookie );
	}

	public function onWpAdminNotices() {
		$oPfx = $this->getPrefix();
		$aNotices = [
			'success' => apply_filters( $oPfx->prefix( 'generate_admin_notices' ), [], 'success' ),
			'warning' => apply_filters( $oPfx->prefix( 'generate_admin_notices' ), [], 'warning' ),
			'danger'  => apply_filters( $oPfx->prefix( 'generate_admin_notices' ), [], 'danger' ),
			'info'    => apply_filters( $oPfx->prefix( 'generate_admin_notices' ), [], 'info' ),
		]; // yes, straight outta bootstrap/compton.

		foreach ( $aNotices as $sKey => $aNoticeCollection ) {
			if ( !empty( $aNoticeCollection ) ) {
				foreach ( $aNoticeCollection as $sNotice ) {
					if ( !empty( $sNotice ) && is_string( $sNotice ) ) {
						echo $sNotice;
					}
				}
			}
		}
		$this->flashNotice();
	}

	protected function flashNotice() {
		if ( !empty( $this->sFlashMessage ) ) {
			echo $this->wrapAdminNoticeHtml( $this->sFlashMessage );
		}
	}

	/**
	 * Provides the basic HTML template for printing a WordPress Admin Notices
	 *
	 * @param $sNotice       - The message to be displayed.
	 * @param $sMessageClass - either error or updated
	 * @param $bPrint        - if true, will echo. false will return the string
	 *
	 * @return bool|string
	 */
	protected function wrapAdminNoticeHtml( $sNotice = '', $sMessageClass = 'updated', $bPrint = false ) {
		$sWrapper = '<div class="%s icwp-admin-notice">%s</div>';
		$sFullNotice = sprintf( $sWrapper, $sMessageClass, $sNotice );
		if ( $bPrint ) {
			echo $sFullNotice;
			return true;
		}
		else {
			return $sFullNotice;
		}
	}
}
