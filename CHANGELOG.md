#### 0.11

- Add support for newer Rest Handling from Plugin Core (used in Shield).

#### 0.9

- Add Logging component to modules base.
